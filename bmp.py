# -*- coding: utf-8 -*-
"""Библиотека для разбора Bmp"""

from logger import Logger
import os
import math


log = Logger()

BITMAP_CORE_BSIZE = 12
BITMAP_V3_HEADER_BSIZE = 40
BITMAP_V4_HEADER_BSIZE = 108
BITMAP_V5_HEADER_BSIZE = 124
FILE_HEADER_SIZE = 14

INT_TO_FXPT2DOT30 = 10 ** 9

REPORT_PATTERNS = {
    'bfType': 'bfType: {}. (The file type; must be BM)\n',
    'bfSize': ('bfSize: {} bytes. (The size, in bytes,'
               ' of the bitmap file)\n'),
    'bfReserved1': ('bfReserved1: {}. (Reserved; must '
                    'be zero)\n'),
    'bfReserved2': ('bfReserved2: {}. (Reserved; must '
                    'be zero)\n'),
    'bfOffBits': ('bfOffBits: {}. (The offset, in bytes, from the beginning '
                  'of the file structure to the bitmap bits)'),
    'bSize': ('bSize: {}. (The number of bytes '
              'required by the structure)\n'),
    'bWidth': ('bWidth: {}. (The width of the bitmap, '
               'in pixels)\n'),
    'bHeight': 'bHeight: {}. (The height of the bitmap, in pixels)\n',
    'bPlanes': ('bPlanes: {}. (The number of planes for the target device; '
                'This value must be 1)\n'),
    'bBitCount': 'bBitCount: {}. (The number of bits-per-pixel)',
    'bCompression': ('bCompression: {}. (The type of compression for'
                     ' a bitmap)\n'),
    'bSizeImage': 'bSizeImage: {}. (The size, in bytes, of the image)\n',
    'bXPelsPerMeter': (
        'bXPelsPerMeter: {}. (The horizontal resolution, in '
        'pixels-per-meter, of the target device for the bitmap)\n'),
    'bYPelsPerMeter': (
        'bYPelsPerMeter: {}. (The horizontal resolution, in '
        'pixels-per-meter, of the target device for the bitmap)\n'),
    'bClrUsed': ('bClrUsed: {}. (The number of color indexes in '
                 'the color table that are actually used by the bitmap.)\n'),
    'bClrImportant': (
        'bClrImportant: {}. (The number of color indexes that are '
        'required for displaying the bitmap)'),
    'bRedMask': ('bRedMask: {}. (Color mask that specifies'
                 ' the red component of each pixel)\n'),
    'bGreenMask': ('bGreenMask: {}. (Color mask that specifies'
                   ' the green component of each pixel)\n'),
    'bBlueMask': ('bBlueMask: {}. (Color mask that specifies'
                  ' the blue component of each pixel)\n'),
    'bAlphaMask': ('bAlphaMask: {}. (Color mask that specifies'
                   ' the alpha component of each pixel)\n'),
    'bCSType': 'bCSType: {}. (The color space of the DIB)\n',
    'bEndpoints': (
        'bEndpoints (A structure that specifies coordinates of the '
        'red, green, and blue endpoints for '
        'the logical color space associated with the bitmap): '
        '\n    Red {}\n    Green {}\n    Blue {}\n'),
    'bGammaRed': 'bGammaRed: {}. (Toned response curve for red)\n',
    'bGammaGreen': 'bGammaGreen: {}. (Toned response curve for green)\n',
    'bGammaBlue': 'bGammaBlue: {}. (Toned response curve for blue)\n',
    'bV5Intent': 'bV5Intent: {}. (Rendering intent for bitmap)\n',
    'bV5ProfileData': (
        'bV5ProfileData: {}. (The offset, in bytes, from the beginning'
        ' of the BITMAPV5HEADER structure to the start of the profile '
        'data)\n'),
    'bV5ProfileSize': (
        'bV5ProfileSize: {}. (Size, in bytes, of embedded profile data)\n'),
    'bV5Reserved': ('bV5Reserved: {}. (This member has been reserved. '
                    'Its value should be set to zero)\n')
}


def get_file_data(file_path):
    """Получение данных из файла"""

    try:
        with open(file_path, 'rb') as file:
            return file.read()
    except FileNotFoundError as error:
        log.logger.error(error)
        return error
    except PermissionError as error:
        log.logger.error(error)
        return error


def get_image(file_path):
    file_path = os.path.abspath(file_path)
    file_data = get_file_data(file_path)
    if not isinstance(file_data, bytes):
        return file_data
    data_bytes_array = bytearray(file_data)
    bmp_version = get_bmp_version(data_bytes_array)
    if isinstance(bmp_version, str):
        return bmp_version
    return bmp_version(data_bytes_array)


def get_bmp_version(image_data):
    """Получение класса для соответствующей версии BMP"""

    is_bmp(image_data[0:2].decode(errors='ignore'))
    b_size = get_value_object(image_data, (14, 18), '')['value']
    if b_size == BITMAP_CORE_BSIZE:
        return BitmapCore
    elif b_size == BITMAP_V3_HEADER_BSIZE:
        return BitmapV3Header
    elif b_size == BITMAP_V4_HEADER_BSIZE:
        return BitmapV4Header
    elif b_size == BITMAP_V5_HEADER_BSIZE:
        return BitmapV5Header
    else:
        message = 'Wrong header size: {}.'.format(b_size)
        log.logger.error(message)
        return message


def get_value_object(byte_array, start_end_indexes, report_message):
    byteorder = 'little'

    start = start_end_indexes[0]
    end = start_end_indexes[1]
    value = int.from_bytes(byte_array[start:end], byteorder=byteorder)
    return {'value': value, 'indexes': start_end_indexes,
            'message': report_message.format(value)}


def get_hex_dump(image_data):
    hex_dump = []
    for byte in image_data:
        hex_dump.append(str.upper(hex(byte)[2:]))
    return hex_dump


def is_bmp(bf_type):
        """Проверка того, что тип картинки - BMP"""

        if bf_type != 'BM':
            message = 'File format not BMP'
            log.logger.error(message)
            raise Exception(message)


class Bmp:
    """Главный класс для BMP версий CORE - V5"""

    def __init__(self, image_data):
        self.image_data = image_data
        self.image_fields_array = []
        self.format_report = ''

        bmp_type = image_data[0:2].decode(errors='ignore')
        self.bfType = {
            'value': bmp_type,
            'indexes': (0, 2),
            'message': REPORT_PATTERNS['bfType'].format(bmp_type)}
        self.bfSize = get_value_object(
            image_data, (2, 6), REPORT_PATTERNS['bfSize'])
        self.bfReserved1 = get_value_object(
            image_data, (6, 8), REPORT_PATTERNS['bfReserved1'])
        self.bfReserved2 = get_value_object(
            image_data, (8, 10), REPORT_PATTERNS['bfReserved2'])
        self.bfOffBits = get_value_object(
            image_data, (10, 14), REPORT_PATTERNS['bfOffBits'])

    def get_image_fields_array(self):
        self.image_fields_array.extend([
            self.bfType, self.bfSize, self.bfReserved1,
            self.bfReserved2, self.bfOffBits])

    def get_format_report(self):
        """Вывод в лог информации о формате"""

        self.format_report += self.bfType['message'].format(
            self.bfType['value'])
        self.format_report += self.bfSize['message'].format(
            self.bfSize['value'])
        self.format_report += self.bfReserved1['message'].format(
            self.bfReserved1['value'])
        self.format_report += self.bfReserved2['message'].format(
            self.bfReserved2['value'])
        self.format_report += self.bfOffBits['message'].format(
            self.bfOffBits['value'])

    def print_fields(self):
        log.logger.info(self.format_report)


class BitmapCore(Bmp):
    """Класс для разбора формата CORE"""

    def __init__(self, image_data):
        super().__init__(image_data[0:14])
        self.image_data = image_data
        self.palette = {
            'value': None, 'message': '', 'indexes': (0, 0)
        }

        self.version = 'BitmapCore'
        self.top_down_flag = False
        self.bSize = get_value_object(
            image_data, (14, 18), REPORT_PATTERNS['bSize'])
        self.bWidth = get_value_object(
            image_data, (18, 20), REPORT_PATTERNS['bWidth'])
        self.bHeight = self.get_height(image_data, (20, 22))
        self.bPlanes = get_value_object(
            image_data, (22, 24), REPORT_PATTERNS['bPlanes'])
        self.bBitCount = get_value_object(
            image_data, (24, 26), REPORT_PATTERNS['bBitCount'])

        bytes_per_line = self.bWidth['value'] * self.bBitCount['value'] / 8
        self.padding = (4 - math.ceil(bytes_per_line) % 4) % 4

        self.bmiColors = None
        self.format_report = ''

    def get_height(self, data, range_indexes):
        if len(range(*range_indexes)) == 2:
            max_value = 65535
            unsigned_part = max_value // 2
        else:
            max_value = 4294967295
            unsigned_part = max_value // 2

        start = range_indexes[0]
        end = range_indexes[-1]
        height_value = int.from_bytes(data[start:end], byteorder='little')
        if height_value > unsigned_part:
            height_value = (height_value - max_value) * -1
            self.top_down_flag = True
        return {'value': height_value, 'indexes': range_indexes,
                'message': REPORT_PATTERNS['bHeight'].format(height_value)}

    def is_palette_in_image(self):
        off_bits = self.bfOffBits['value']
        b_size = self.bSize['value']
        return off_bits - b_size - FILE_HEADER_SIZE > 0

    def get_image_fields_array(self):
        Bmp.get_image_fields_array(self)
        self.image_fields_array.extend([
            self.bSize, self.bWidth, self.bHeight,
            self.bPlanes, self.bBitCount, self.palette])

    def get_format_report(self):
        """Вывод полей в лог и консоль"""

        self.format_report += '\nVersion: {}\n'.format(
            self.version)
        Bmp.get_format_report(self)
        self.format_report += '\n\n' + self.bSize['message'].format(
            self.bSize['value'])
        self.format_report += self.bWidth['message'].format(
            self.bWidth['value'])
        self.format_report += self.bHeight['message'].format(
            self.bHeight['value'])
        self.format_report += self.bPlanes['message'].format(
            self.bPlanes['value'])
        self.format_report += self.bBitCount['message'].format(
            self.bBitCount['value'])

    def get_palette(self):
        start = FILE_HEADER_SIZE + self.bSize['value']
        palette_bytes = self.image_data[start:self.bfOffBits['value']]
        palette_values = []

        while palette_bytes:
            color = palette_bytes[:4]
            color_tuple = (*(byte for byte in color),)
            if color_tuple[3] == 0:
                color_tuple = (*color_tuple[:3], 255)
            palette_values.append((*reversed(color_tuple[:3]), color_tuple[3]))
            palette_bytes = palette_bytes[4:]
        self.palette = {
            'value': palette_values,
            'indexes': (start, self.bfOffBits['value'])}

    def get_bmi_colors(self):
        """Определение битности пикселей и получение таблицы пикселей"""

        pixels = self.image_data[self.bfOffBits['value']:]
        bit_count = self.bBitCount['value']
        if bit_count in [1, 4, 8]:
            self.bmiColors = self.handle_small_bit_counts(pixels)
        elif bit_count == 16:
            self.bmiColors = self.handle_16_bit_count(pixels)
        elif bit_count in [24, 32]:
            self.bmiColors = self.handle_three_byte_channels_pixels(pixels)

    def get_pixel_color_from_palette(self, palette_index):
        if palette_index >= len(self.palette['value']):
            palette_index = len(self.palette['value']) - 1
        color = self.palette['value'][palette_index]
        return color

    def handle_small_bit_counts(self, pixels_data):
        """Получение пикселей для битностей  1, 4, 8"""

        pixels = []
        step = 8 // self.bBitCount['value']
        pixels_in_current_line = 0
        last_bits = get_last_bits(self.bBitCount['value'])
        index = 0

        while index < len(pixels_data):
            byte = pixels_data[index]
            index += 1

            for i in range(step):
                pixel = (byte >> (i * (8 // step))) & last_bits
                pixels.append((pixel,))
                pixels_in_current_line += 1

                if pixels_in_current_line == self.bWidth['value']:
                    pixels_in_current_line = 0
                    index += self.padding
                    break

        return pixels

    def handle_16_bit_count(self, pixels_data):
        """Получение пикселей для битности 16"""

        pixels = []
        index = 0

        for index_y in range(self.bHeight['value']):
            for index_x in range(self.bWidth['value']):
                byte_pixel = int.from_bytes(
                    pixels_data[index:index + 2],
                    byteorder='little')
                index += 2
                red = (byte_pixel & 0xF800) >> 11 << 3
                green = (byte_pixel & 0x7E0) >> 5 << 2
                blue = (byte_pixel & 0x1F) << 3
                pixel = [red, green, blue]
                pixels.append(tuple(reversed(pixel)))

            index += self.padding

        return pixels

    def handle_three_byte_channels_pixels(self, pixels_data):
        """Получение пикселей для битности 24, 32"""

        pixels = []
        bytes_per_pixel = self.bBitCount['value'] // 8
        padding = self.padding
        index = 0

        for index_y in range(self.bHeight['value']):
            for index_x in range(self.bWidth['value']):
                pixel_bytes = list(pixels_data[index:index + bytes_per_pixel])
                index += bytes_per_pixel
                if bytes_per_pixel == 4 and pixel_bytes[3] == 0:
                    pixel_bytes = [*pixel_bytes[:3], 255]
                pixel_bytes[:3] = reversed(pixel_bytes[:3])
                pixels.append(tuple(pixel_bytes))
            index += padding

        return pixels


def get_last_bits(bit_count):
    """Получение маски для данной битности"""

    if bit_count == 1:
        last_bits = 1
    elif bit_count == 4:
        last_bits = 15
    else:
        last_bits = 255

    return last_bits


class BitmapV3Header(BitmapCore):
    """Класс для разбора V3"""

    def __init__(self, image_data):
        super().__init__(image_data)

        self.version = 'BitmapV3Header'
        self.bSize = get_value_object(
            image_data, (14, 18), REPORT_PATTERNS['bSize'])
        self.bWidth = get_value_object(
            image_data, (18, 22), REPORT_PATTERNS['bWidth'])
        self.bHeight = self.get_height(image_data, (22, 26))
        self.bPlanes = get_value_object(
            image_data, (26, 28), REPORT_PATTERNS['bPlanes'])
        self.bBitCount = get_value_object(
            image_data, (28, 30), REPORT_PATTERNS['bBitCount'])
        self.bCompression = get_value_object(
            image_data, (30, 34), REPORT_PATTERNS['bCompression'])
        self.bSizeImage = get_value_object(
            image_data, (34, 38), REPORT_PATTERNS['bSizeImage'])
        self.bXPelsPerMeter = get_value_object(
            image_data, (38, 42), REPORT_PATTERNS['bXPelsPerMeter'])
        self.bYPelsPerMeter = get_value_object(
            image_data, (42, 46), REPORT_PATTERNS['bYPelsPerMeter'])
        self.bClrUsed = get_value_object(
            image_data, (46, 50), REPORT_PATTERNS['bClrUsed'])
        self.bClrImportant = get_value_object(
            image_data, (50, 54), REPORT_PATTERNS['bClrImportant'])
        bytes_per_line = self.bWidth['value'] * self.bBitCount['value'] / 8
        self.padding = (4 - math.ceil(bytes_per_line) % 4) % 4

        self.format_report = ''

    def get_image_fields_array(self):
        BitmapCore.get_image_fields_array(self)
        self.image_fields_array.extend([
            self.bCompression, self.bSizeImage, self.bXPelsPerMeter,
            self.bYPelsPerMeter, self.bClrUsed, self.bClrImportant])

    def get_format_report(self):
        """Вывод разбора в лог и консоль"""

        BitmapCore.get_format_report(self)
        self.format_report += '\n\n' + self.bCompression['message'].format(
            self.bCompression['value'])
        self.format_report += self.bSizeImage['message'].format(
            self.bSizeImage['value'])
        self.format_report += self.bXPelsPerMeter['message'].format(
            self.bXPelsPerMeter['value'])
        self.format_report += self.bYPelsPerMeter['message'].format(
            self.bYPelsPerMeter['value'])
        self.format_report += self.bClrUsed['message'].format(
            self.bClrUsed['value'])
        self.format_report += self.bClrImportant['message'].format(
            self.bClrImportant['value'])


class BitmapV4Header(BitmapV3Header):
    """Класс для разбора V4"""

    def __init__(self, image_data):
        super().__init__(image_data)

        self.version = 'BitmapV4Header'
        self.bRedMask = get_value_object(
            image_data, (54, 58), REPORT_PATTERNS['bRedMask'])
        self.bGreenMask = get_value_object(
            image_data, (58, 62), REPORT_PATTERNS['bGreenMask'])
        self.bBlueMask = get_value_object(
            image_data, (62, 66), REPORT_PATTERNS['bBlueMask'])
        self.bAlphaMask = get_value_object(
            image_data, (66, 70), REPORT_PATTERNS['bAlphaMask'])

        cstype_value = image_data[70:74].decode()[::-1]
        self.bCSType = {
            'value': cstype_value, 'indexes': (70, 74),
            'message': REPORT_PATTERNS['bCSType'].format(cstype_value)}

        endpoints_values = get_end_points(image_data[74:110])
        self.bEndpoints = {
            'value': endpoints_values,
            'indexes': (74, 110),
            'message': REPORT_PATTERNS['bEndpoints'].format(
                *endpoints_values)}
        self.bGammaRed = get_value_object(
            image_data, (110, 114), REPORT_PATTERNS['bGammaRed'])
        self.bGammaGreen = get_value_object(
            image_data, (114, 118), REPORT_PATTERNS['bGammaGreen'])
        self.bGammaBlue = get_value_object(
            image_data, (118, 122), REPORT_PATTERNS['bGammaBlue'])

    def get_image_fields_array(self):
        BitmapV3Header.get_image_fields_array(self)
        self.image_fields_array.extend([
            self.bRedMask, self.bGreenMask, self.bBlueMask, self.bAlphaMask,
            self.bCSType, self.bEndpoints, self.bGammaRed,
            self.bGammaGreen, self.bGammaBlue])

    def get_format_report(self):
        """Вывод разбора в лог и консоль"""

        BitmapV3Header.get_format_report(self)
        self.format_report += '\n\n' + self.bRedMask['message'].format(
            self.bRedMask['value'])
        self.format_report += self.bGreenMask['message'].format(
            self.bGreenMask['value'])
        self.format_report += self.bBlueMask['message'].format(
            self.bBlueMask['value'])
        self.format_report += self.bAlphaMask['message'].format(
            self.bAlphaMask['value'])
        self.format_report += self.bCSType['message'].format(
            self.bCSType['value'])
        self.format_report += self.bEndpoints['message'].format(
            *self.bEndpoints['value'])
        self.format_report += self.bGammaRed['message'].format(
            self.bGammaRed['value'])
        self.format_report += self.bGammaGreen['message'].format(
            self.bGammaGreen['value'])
        self.format_report += self.bGammaBlue['message'].format(
            self.bGammaBlue['value'])


def get_end_points(b_end_points):
    """Получение координат конечных цветовых точек"""

    red = b_end_points[0:12]
    green = b_end_points[12:24]
    blue = b_end_points[24:36]
    ciexyz_red = tuple(format_end_points(red))
    ciexyz_green = tuple(format_end_points(green))
    ciexyz_blue = tuple(format_end_points(blue))
    return ciexyz_red, ciexyz_green, ciexyz_blue


def format_end_points(color):
    """Форматирование координат конечных цветовых точек"""

    start = 0
    end = 4
    coordinates = []

    for index in range(3):
        coordinates.append(
            int.from_bytes(color[start:end], 'little') / INT_TO_FXPT2DOT30)
        start += 4
        end += 4

    return coordinates


class BitmapV5Header(BitmapV4Header):
    """Класс для разбора V5"""

    def __init__(self, image_data):
        super().__init__(image_data)

        self.version = 'BitmapV5Header'
        self.bV5Intent = get_value_object(
            image_data, (122, 126), REPORT_PATTERNS['bV5Intent'])
        self.bV5ProfileData = get_value_object(
            image_data, (126, 130), REPORT_PATTERNS['bV5ProfileData'])
        self.bV5ProfileSize = get_value_object(
            image_data, (130, 134), REPORT_PATTERNS['bV5ProfileSize'])
        self.bV5Reserved = get_value_object(
            image_data, (134, 138), REPORT_PATTERNS['bV5Reserved'])

    def get_image_fields_array(self):
        BitmapV4Header.get_image_fields_array(self)
        self.image_fields_array.extend([
            self.bV5Intent, self.bV5ProfileData,
            self.bV5ProfileSize, self.bV5Reserved])

    def get_format_report(self):
        """Вывод разбора в лог и консоль"""

        BitmapV4Header.get_format_report(self)
        self.format_report += '\n\n' + self.bV5Intent['message'].format(
            self.bV5Intent['value'])
        self.format_report += self.bV5ProfileData['message'].format(
            self.bV5ProfileData['value'])
        self.format_report += self.bV5ProfileSize['message'].format(
            self.bV5ProfileSize['value'])
        self.format_report += self.bV5Reserved['message'].format(
            self.bV5Reserved['value'])
