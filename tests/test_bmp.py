# -*- coding: utf-8 -*-
"""Тесты для разбора BMP"""


import unittest
import os
import sys


PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.realpath(os.path.dirname(__file__))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))


import bmp


def get_abs_path(path_points):
    return os.path.abspath(
        os.path.join(os.path.split(SCRIPT_DIR)[0], 'testSuite', *path_points))


G_V3_16BIT = get_abs_path(['V3', 'good', 'rgb16.bmp'])
G_V3_32BIT = get_abs_path(['V3', 'good', 'rgb32.bmp'])
G_V3_4BIT = get_abs_path(['V3', 'good', 'pal4.bmp'])
Q_V5 = get_abs_path(['V5', 'questionable', 'rgb24prof.bmp'])
WRONG_HEADER_FILE = get_abs_path(['wrongHeaderSize', 'rgb32h52.bmp'])
G_V3 = get_abs_path(['V3', 'good', 'pal4.bmp'])
G_V4 = get_abs_path(['V4', 'good', 'pal8v4.bmp'])
G_V5 = get_abs_path(['V5', 'good', 'pal8v5.bmp'])
Q_OS2 = get_abs_path(['CORE', 'questionable', 'pal8os2sp.bmp'])


class TestBMP(unittest.TestCase):

    def test_g_os2_analysis(self):
        os2_bytes = get_byte_from_file(Q_OS2)
        version_class = bmp.get_bmp_version(os2_bytes)
        image = version_class(os2_bytes)
        self.assertTrue(isinstance(image, bmp.BitmapCore))

        self.assertEqual('BitmapCore', image.version)
        self.assertEqual('BM', image.bfType['value'])
        self.assertEqual(8974, image.bfSize['value'])
        self.assertEqual(0, image.bfReserved1['value'])
        self.assertEqual(0, image.bfReserved2['value'])
        self.assertEqual(782, image.bfOffBits['value'])
        self.assertEqual(12, image.bSize['value'])
        self.assertEqual(127, image.bWidth['value'])
        self.assertEqual(64, image.bHeight['value'])
        self.assertEqual(1, image.bPlanes['value'])
        self.assertEqual(8, image.bBitCount['value'])

    def test_g_v3_analysis(self):
        v3_bytes = get_byte_from_file(G_V3)
        version_class = bmp.get_bmp_version(v3_bytes)
        image = version_class(v3_bytes)

        self.assertTrue(isinstance(image, bmp.BitmapV3Header))
        self.assertEqual('BitmapV3Header', image.version)
        self.assertEqual('BM', image.bfType['value'])
        self.assertEqual(4198, image.bfSize['value'])
        self.assertEqual(0, image.bfReserved1['value'])
        self.assertEqual(0, image.bfReserved2['value'])
        self.assertEqual(102, image.bfOffBits['value'])
        self.assertEqual(40, image.bSize['value'])
        self.assertEqual(127, image.bWidth['value'])
        self.assertEqual(64, image.bHeight['value'])
        self.assertEqual(1, image.bPlanes['value'])
        self.assertEqual(4, image.bBitCount['value'])
        self.assertEqual(0, image.bCompression['value'])
        self.assertEqual(4096, image.bSizeImage['value'])
        self.assertEqual(2835, image.bXPelsPerMeter['value'])
        self.assertEqual(2835, image.bYPelsPerMeter['value'])
        self.assertEqual(12, image.bClrUsed['value'])
        self.assertEqual(0, image.bClrImportant['value'])

    def test_g_v4_analysis(self):
        v4_bytes = get_byte_from_file(G_V4)
        version_class = bmp.get_bmp_version(v4_bytes)
        image = version_class(v4_bytes)
        self.assertTrue(isinstance(image, bmp.BitmapV4Header))

        self.assertEqual('BitmapV4Header', image.version)
        self.assertEqual('BM', image.bfType['value'])
        self.assertEqual(9322, image.bfSize['value'])
        self.assertEqual(0, image.bfReserved1['value'])
        self.assertEqual(0, image.bfReserved2['value'])
        self.assertEqual(1130, image.bfOffBits['value'])
        self.assertEqual(108, image.bSize['value'])
        self.assertEqual(127, image.bWidth['value'])
        self.assertEqual(64, image.bHeight['value'])
        self.assertEqual(1, image.bPlanes['value'])
        self.assertEqual(8, image.bBitCount['value'])
        self.assertEqual(0, image.bCompression['value'])
        self.assertEqual(8192, image.bSizeImage['value'])
        self.assertEqual(2835, image.bXPelsPerMeter['value'])
        self.assertEqual(2835, image.bYPelsPerMeter['value'])
        self.assertEqual(252, image.bClrUsed['value'])
        self.assertEqual(0, image.bClrImportant['value'])
        self.assertEqual(0, image.bRedMask['value'])
        self.assertEqual(0, image.bGreenMask['value'])
        self.assertEqual(0, image.bBlueMask['value'])
        self.assertEqual(0, image.bAlphaMask['value'])
        self.assertEqual('\x00\x00\x00\x00', image.bCSType['value'])
        self.assertEqual(
            (0.687194767, 0.354334802, 0.032212255),
            image.bEndpoints['value'][0])
        self.assertEqual(
            (0.322122547, 0.644245094, 0.107374182),
            image.bEndpoints['value'][1])
        self.assertEqual(
            (0.161061274, 0.064424509, 0.848256041),
            image.bEndpoints['value'][2])
        self.assertEqual(29789, image.bGammaRed['value'])
        self.assertEqual(29789, image.bGammaGreen['value'])
        self.assertEqual(29789, image.bGammaBlue['value'])

    def test_g_v5_analysis(self):
        v5_bytes = get_byte_from_file(G_V5)
        version_class = bmp.get_bmp_version(v5_bytes)
        image = version_class(v5_bytes)

        self.assertTrue(isinstance(image, bmp.BitmapV5Header))
        self.assertEqual('BitmapV5Header', image.version)
        self.assertEqual('BM', image.bfType['value'])
        self.assertEqual(9338, image.bfSize['value'])
        self.assertEqual(0, image.bfReserved1['value'])
        self.assertEqual(0, image.bfReserved2['value'])
        self.assertEqual(1146, image.bfOffBits['value'])
        self.assertEqual(124, image.bSize['value'])
        self.assertEqual(127, image.bWidth['value'])
        self.assertEqual(64, image.bHeight['value'])
        self.assertEqual(1, image.bPlanes['value'])
        self.assertEqual(8, image.bBitCount['value'])
        self.assertEqual(0, image.bCompression['value'])
        self.assertEqual(8192, image.bSizeImage['value'])
        self.assertEqual(2835, image.bXPelsPerMeter['value'])
        self.assertEqual(2835, image.bYPelsPerMeter['value'])
        self.assertEqual(252, image.bClrUsed['value'])
        self.assertEqual(0, image.bClrImportant['value'])
        self.assertEqual(0, image.bRedMask['value'])
        self.assertEqual(0, image.bGreenMask['value'])
        self.assertEqual(0, image.bBlueMask['value'])
        self.assertEqual(0, image.bAlphaMask['value'])
        self.assertEqual(image.bCSType['value'], 'sRGB')
        self.assertEqual((0, 0, 0), image.bEndpoints['value'][0])
        self.assertEqual((0, 0, 0), image.bEndpoints['value'][1])
        self.assertEqual((0, 0, 0), image.bEndpoints['value'][2])
        self.assertEqual(0, image.bGammaRed['value'])
        self.assertEqual(0, image.bGammaGreen['value'])
        self.assertEqual(0, image.bGammaBlue['value'])
        self.assertEqual(4, image.bV5Intent['value'])
        self.assertEqual(0, image.bV5ProfileData['value'])
        self.assertEqual(0, image.bV5ProfileSize['value'])
        self.assertEqual(0, image.bV5Reserved['value'])

    def test_raised_exception_get_version(self):
        image_bytes = get_byte_from_file(WRONG_HEADER_FILE)
        answer = bmp.get_bmp_version(image_bytes)
        self.assertTrue('Wrong header size' in answer)

    def test_is_bmp(self):
        bytes_array = bytearray(b'asdqwertywercxdsfsdf')
        bmp_obj = bmp.Bmp(bytes_array)
        with self.assertRaises(Exception):
            bmp.is_bmp(bmp_obj.bfType['value'])

    def test_get_hex_dump(self):
        b_string = b'BM'
        self.assertEqual(['42', '4D'], bmp.get_hex_dump(b_string))

    def test_not_bmp_exception(self):
        with self.assertRaises(Exception):
            bmp.is_bmp('AS')

    def test_format_report_bmp(self):
        os2_bytes = get_byte_from_file(Q_OS2)
        version_class = bmp.get_bmp_version(os2_bytes)
        image = version_class(os2_bytes)
        image.get_format_report()

        format_fields = [
            'bfType', 'bfSize', 'bfReserved1', 'bfReserved2',
            'bfOffBits', 'bSize', 'bWidth', 'bHeight', 'bPlanes', 'bBitCount']

        for field in format_fields:
            self.assertTrue(field in image.format_report)

    def test_get_image(self):
        image = bmp.get_image(G_V3_4BIT)
        self.assertTrue(isinstance(image, bmp.BitmapV3Header))

    def test_g_v3_4bit_count(self):
        v3_bytes = get_byte_from_file(G_V3_4BIT)
        version_class = bmp.get_bmp_version(v3_bytes)
        image = version_class(v3_bytes)
        self.assertEqual(4, image.bBitCount['value'])

    def test_g_v3_32bit_count(self):
        v3_bytes = get_byte_from_file(G_V3_32BIT)
        version_class = bmp.get_bmp_version(v3_bytes)
        image = version_class(v3_bytes)
        self.assertEqual(
            image.bWidth['value'] * image.bHeight['value'] * 4,
            image.bSizeImage['value'])

    def test_g_v3_16bit_count(self):
        v3_bytes = get_byte_from_file(G_V3_16BIT)
        version_class = bmp.get_bmp_version(v3_bytes)
        image = version_class(v3_bytes)
        self.assertEqual(
            (image.bWidth['value'] + 1) * image.bHeight['value'] * 2,
            image.bSizeImage['value'])

    def test_is_palette_in_image(self):
        v3_bytes = get_byte_from_file(G_V3_16BIT)
        version_class = bmp.get_bmp_version(v3_bytes)
        image = version_class(v3_bytes)
        self.assertFalse(image.is_palette_in_image())

    def test_wrong_file_opening(self):
        answer = bmp.get_file_data('sadfsadf')
        self.assertTrue(isinstance(answer, FileNotFoundError))

    def test_get_palette(self):
        v3_bytes = get_byte_from_file(G_V3)
        version_class = bmp.get_bmp_version(v3_bytes)
        image = version_class(v3_bytes)
        image.get_palette()

        start = 14 + image.bSize['value']
        end = image.bfOffBits['value']
        self.assertEqual(image.palette['indexes'], (start, end))

    def test_get_bmi_colors(self):
        v3_bytes = get_byte_from_file(G_V3)
        version_class = bmp.get_bmp_version(v3_bytes)
        image = version_class(v3_bytes)
        image.get_bmi_colors()

        pixels_count = image.bWidth['value'] * image.bHeight['value']
        self.assertEqual(len(image.bmiColors), pixels_count)

    def test_get_format_report(self):
        v3_bytes = get_byte_from_file(G_V5)
        version_class = bmp.get_bmp_version(v3_bytes)
        image = version_class(v3_bytes)
        image.get_format_report()

        self.assertEqual(image.version, 'BitmapV5Header')

    def test_3channels_image_pixels(self):
        v3_bytes = get_byte_from_file(G_V3_32BIT)
        version_class = bmp.get_bmp_version(v3_bytes)
        image = version_class(v3_bytes)

        image.get_bmi_colors()
        pixels_count = image.bWidth['value'] * image.bHeight['value']
        self.assertEqual(len(image.bmiColors), pixels_count)


def get_byte_from_file(file_path):
    file_path = os.path.abspath(file_path)
    with open(file_path, 'rb') as file:
        file_data = file.read()

    return bytearray(file_data)


if __name__ == '__main__':
    unittest.main()
