# -*- coding: utf-8 -*-
"""Модуль для запуска разбора формата BMP"""


import argparse
import sys


import bmp


def main():
    """Точка входа в программу"""

    args = parse_args()
    file_path = args.file_path

    image = bmp.get_image(file_path)
    if not isinstance(image, bmp.Bmp):
        sys.exit(image)
    image.get_format_report()
    image.print_fields()


def parse_args():
    """Парсинг аргументов командной строки"""

    description = "Консольная версия программы для разбора формата BMP"

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('file_path')

    return parser.parse_args()

if __name__ == '__main__':
    main()
