# -*- coding: utf-8 -*-
"""Модуль с классом для записи логов"""


import logging
import time
import os


class Logger:

    def __init__(self):
        self.logger = logging.getLogger('bmp')
        self.logger.setLevel(logging.INFO)

        date = time.ctime()
        date_str = str.replace(date, ':', '-')
        executed_dir = os.path.split(os.path.realpath('test_bmp.py'))[0]
        full_path = os.path.abspath(
            os.path.join(executed_dir, 'logs', date_str + '.log'))

        logs_dir = os.path.split(full_path)[0]
        if not os.path.exists(logs_dir):
            os.makedirs(logs_dir)

        self.fh = logging.FileHandler(filename=full_path, encoding='utf8')
        self.fh.setLevel(logging.DEBUG)

        self.ch = logging.StreamHandler()

        self.logger.addHandler(self.ch)
        self.logger.addHandler(self.fh)
