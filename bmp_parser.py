"""Графическая версия разбора BMP"""

import os
import sys
import bmp
import math
import argparse


try:
    from PyQt5 import QtWidgets, QtGui, QtCore
except ImportError as err:
    QtWidgets = None
    QtCore = None
    QtGui = None
    sys.exit(err)


FILE_HEADER_COLOR = QtGui.QColor(239, 75, 75)
INFO_HEADER_COLOR = QtGui.QColor(128, 255, 75)


class Thread(QtCore.QThread):
    set_cell_signal = QtCore.pyqtSignal(int, int, QtWidgets.QTableWidgetItem)
    progress_bar_signal = QtCore.pyqtSignal(str)
    draw_pixels_line_signal = QtCore.pyqtSignal()

    def __init__(self, main_window):
        super().__init__()
        self.image_info = main_window.parsed_image
        self.image = main_window.image
        self.table_columns_count = main_window.hex_table_columns_count
        self.hex_table = main_window.hex_table
        self.need_to_stop = False
        self.row_index = 0
        self.column_index = 0
        self.get_color = None
        self.define_get_color_function()

        self.previous_update = 0
        self.width = self.image_info.bWidth['value']
        self.height = self.image_info.bHeight['value']

    def run(self):
        self.image_info.get_image_fields_array()
        if self.image_info.is_palette_in_image():
            self.image_info.get_palette()
        self.progress_bar_signal.emit('Parsing pixels')
        self.image_info.get_bmi_colors()
        hex_dump = bmp.get_hex_dump(self.image_info.image_data)
        self.create_cells_for_table(hex_dump[:14], FILE_HEADER_COLOR)
        self.create_cells_for_table(
            hex_dump[14:self.image_info.bSize['value'] + 14],
            INFO_HEADER_COLOR)

        self.draw_palette(hex_dump)
        self.draw_pixels(hex_dump)

    def define_get_color_function(self):
        if self.image_info.bBitCount['value'] <= 8:
            self.get_color = lambda pixel: self.get_palette_pixel_color(pixel)
        else:
            self.get_color = lambda pixel: get_non_palette_pixel_color(pixel)

    def get_palette_pixel_color(self, pixel):
        color = self.image_info.get_pixel_color_from_palette(pixel[0])
        return QtGui.QColor(*color)

    def draw_palette(self, hex_dump):
        if self.image_info.palette['value']:
            indexes = self.image_info.palette['indexes']
            palette_dump = hex_dump[indexes[0]:indexes[1]]
            for palette_color in self.image_info.palette['value']:
                color_dump = palette_dump[:4]
                palette_dump = palette_dump[4:]
                color = QtGui.QColor(*palette_color)
                self.create_cells_for_table(color_dump, color)

    def draw_pixels(self, hex_dump):
        pixels_dump = hex_dump[self.image_info.bfOffBits['value']:]
        bit_count = self.image_info.bBitCount['value']
        pixels = self.image_info.bmiColors
        bytes_count = math.ceil(bit_count / 8)
        previous_update = 0

        iterations_count = get_iterations_count(len(pixels), bit_count)

        for pixel_index in range(iterations_count):
            index = pixel_index * bytes_count
            pixel_bytes = pixels_dump[index - bytes_count:index]

            if self.need_to_stop:
                break

            color = self.get_color(pixels[pixel_index])
            self.create_cells_for_table(pixel_bytes, color)
            self.draw_part_of_pixels(pixels, pixel_index, bit_count)

            if pixel_index - previous_update > self.width:
                previous_update += self.width
                self.draw_pixels_line_signal.emit()

    def draw_part_of_pixels(self, pixels, index, bit_count):
        if bit_count <= 8:
            index = index
            iters_count = 8 // bit_count
            for x in range(iters_count):
                pixel_index = index * iters_count + x
                self.set_pixel_to_image(pixels, pixel_index)
        else:
            self.set_pixel_to_image(pixels, index)

    def set_pixel_to_image(self, pixels, index):
        x_coord_in_image = index % self.width

        if self.image_info.top_down_flag:
            y_coord_in_image = index // self.width
        else:
            y_coord_in_image = self.height - (index // self.width) - 1

        color = self.get_color(pixels[index])
        self.image.setPixel(
            x_coord_in_image, y_coord_in_image, color.rgba())

    def create_cells_for_table(self, hex_dump, color):
        for sign in hex_dump:
            if len(sign) == 1:
                sign = '0' + sign
            self.set_cell(sign, color)

            self.column_index += 1

            if self.column_index == self.table_columns_count:
                self.column_index = 0
                self.row_index += 1

    def set_cell(self, sign, color):
        cell = QtWidgets.QTableWidgetItem(sign)
        cell.setBackground(color)
        cell.setTextAlignment(QtCore.Qt.AlignCenter)
        index = self.row_index * self.table_columns_count + self.column_index
        if index - self.previous_update > 1000:
            message = '{} cells from {} loaded to table'.format(
                index, len(self.image_info.image_data))
            self.progress_bar_signal.emit(message)
            self.previous_update = index
        self.hex_table.setItem(self.row_index, self.column_index, cell)


def get_iterations_count(pixels_data_length, bit_count):
    if bit_count <= 8:
        return pixels_data_length // (8 // bit_count)
    else:
        return pixels_data_length


def get_non_palette_pixel_color(pixel):
    return QtGui.QColor(*pixel)


class HexDumpTable(QtWidgets.QTableWidget):
    set_info_about_cell_signal = QtCore.pyqtSignal(int, int)
    highlight_cells_signal = QtCore.pyqtSignal(int, int)

    def __init__(self):
        super().__init__()
        self.cellClicked.connect(self.get_info_about_cell)
        self.highlight_cells_signal.connect(self.highlight_cells)

        self.highlighted_cells = []

    def get_info_about_cell(self, row, column):
        self.set_info_about_cell_signal.emit(row, column)

    def highlight_cells(self, start, end):
        if self.is_already_highlighted(start):
            return

        if self.highlighted_cells:
            self.deactivate_previous_highlight()

        for index in range(start, end):
            row = index // self.columnCount()
            column = index % self.columnCount()
            cell = self.item(row, column)
            if cell is None:
                break
            color = cell.background().color()
            rgba_color = (color.red(), color.green(),
                          color.blue(), color.alpha())
            cell.setBackground(QtGui.QColor(94, 155, 255))
            self.highlighted_cells.append((row, column, rgba_color))

    def deactivate_previous_highlight(self):
        for cell in self.highlighted_cells:
            item = self.item(*cell[:2])
            if item is None:
                break
            item.setBackground(QtGui.QColor(*cell[2]))
        self.highlighted_cells = []

    def is_already_highlighted(self, current_cell_index):
        if not self.highlighted_cells:
            return False

        first_highligthed_cell = self.highlighted_cells[0]
        last_highligthed_cell = self.highlighted_cells[-1]

        first_cell_index = self.get_cell_index(first_highligthed_cell)
        last_cell_index = self.get_cell_index(last_highligthed_cell)

        return current_cell_index in range(first_cell_index, last_cell_index)

    def get_cell_index(self, cell):
        return cell[0] * self.columnCount() + cell[1]


class MainWindow(QtWidgets.QWidget):
    def __init__(self, image_path):
        super().__init__()

        self.parsed_image = None
        self.image_path = image_path
        self.thread = None
        self.image_pixmap = None
        self.image = None
        self.hex_table_columns_count = 16

        self.image_label = QtWidgets.QLabel()
        self.image_label.setAlignment(QtCore.Qt.AlignCenter)
        self.image_label.hide()

        self.image_scroll_area = QtWidgets.QScrollArea()
        self.image_scroll_area.setWidgetResizable(True)
        self.image_scroll_area.setWidget(self.image_label)

        self.choose_file_btn = QtWidgets.QPushButton('Choose image')
        self.choose_file_btn.clicked.connect(self.parse_image)

        self.report_messages_box = QtWidgets.QListWidget()
        self.report_messages_box.hide()

        self.cell_info_label = QtWidgets.QLabel()
        self.color_label = QtWidgets.QLabel()
        self.color_label.setStyleSheet("border: 1px solid black")
        self.color_label.setMaximumWidth(40)
        self.color_label.hide()
        self.progress_label = QtWidgets.QLabel()

        self.info_layout = QtWidgets.QVBoxLayout()
        self.info_layout.addWidget(self.image_scroll_area)
        self.info_layout.addWidget(self.report_messages_box)
        self.info_layout.addWidget(self.cell_info_label)
        self.info_layout.addWidget(self.color_label)
        self.info_layout.addWidget(self.progress_label)
        self.info_widget = QtWidgets.QWidget()
        self.info_widget.setLayout(self.info_layout)

        self.file_header_color_label = QtWidgets.QLabel()
        set_color_to_label(self.file_header_color_label, FILE_HEADER_COLOR)
        self.file_header_label = QtWidgets.QLabel('file header part color')

        self.info_header_color_label = QtWidgets.QLabel()
        set_color_to_label(self.info_header_color_label, INFO_HEADER_COLOR)
        self.info_header_label = QtWidgets.QLabel('info header part color')

        self.hex_table = HexDumpTable()
        self.hex_table.horizontalHeader().setSectionResizeMode(
            QtWidgets.QHeaderView.Stretch)
        self.hex_table.set_info_about_cell_signal.connect(self.get_cell_info)
        self.hex_table.setStyleSheet(
            'font-family: Helvetica; font-size: 10px')
        self.hex_table.setEditTriggers(
            QtWidgets.QAbstractItemView.NoEditTriggers)
        self.hex_table.setSelectionMode(
            QtWidgets.QAbstractItemView.SingleSelection)

        self.quit_button = QtWidgets.QPushButton('Quit')
        self.quit_button.clicked.connect(
            QtCore.QCoreApplication.instance().quit)

        self.main_layout = QtWidgets.QVBoxLayout()
        self.grid_layout = QtWidgets.QGridLayout()
        self.hex_grid_layout = QtWidgets.QVBoxLayout()

        self.hex_info_layout = QtWidgets.QFormLayout()
        self.hex_info_layout.addRow(
            self.file_header_color_label, self.file_header_label)
        self.hex_info_layout.addRow(
            self.info_header_color_label, self.info_header_label)

        self.hex_info_widget = QtWidgets.QWidget()
        self.hex_info_widget.setLayout(self.hex_info_layout)

        self.hex_grid_layout.addWidget(self.hex_info_widget)
        self.hex_grid_layout.addWidget(self.hex_table)
        self.hex_widget = QtWidgets.QWidget()
        self.hex_widget.setLayout(self.hex_grid_layout)

        self.main_layout.addWidget(self.choose_file_btn)
        self.grid_layout.addWidget(self.info_widget, 0, 0)
        self.grid_layout.addWidget(self.hex_widget, 0, 1)

        self.grid_widget = QtWidgets.QWidget()
        self.grid_widget.setLayout(self.grid_layout)

        self.main_layout.addWidget(self.grid_widget)
        self.main_layout.addWidget(self.quit_button)
        self.setLayout(self.main_layout)

        if self.image_path:
            self.parse_image()

    def get_cell_info(self, row, column):
        index = row * self.hex_table_columns_count + column
        image_data_length = len(self.parsed_image.image_data)
        pixels_range = range(
            self.parsed_image.bfOffBits['value'], image_data_length)
        palette_range = range(
            bmp.FILE_HEADER_SIZE + self.parsed_image.bSize['value'],
            self.parsed_image.bfOffBits['value'])

        if index >= image_data_length:
            return

        if index in pixels_range:
            self.draw_pixel(row, column)
            start, end = self.get_pixel_indexes(index)
        elif index in palette_range:
            start, end = self.get_palette_indexes(index, row, column)
        else:
            self.color_label.clear()
            self.color_label.hide()
            start, end = self.get_info_cell_indexes(row, column, index)

        self.hex_table.highlight_cells_signal.emit(start, end)

    def get_info_cell_indexes(self, row, column, index):
        for field in self.parsed_image.image_fields_array:
            start = field['indexes'][0]
            end = field['indexes'][1]
            if index in range(start, end):
                self.cell_info_label.show()
                self.cell_info_label.setText(
                    'Cell positon: ({}, {})\n'
                    'Index of byte in file: {}\n{}'.format(
                        row + 1, column + 1, index, field['message']))
                return start, end

    def get_palette_indexes(self, index, row, column):
        b_size = self.parsed_image.bSize['value']
        index_in_palette_array = (index - bmp.FILE_HEADER_SIZE - b_size) // 4
        cell = self.hex_table.item(row, column)
        if not cell:
            return
        color = cell.background().color()
        tuple_color = (color.red(), color.green(),
                       color.blue(), color.alpha())
        self.draw_palette_color(tuple_color)
        start = bmp.FILE_HEADER_SIZE + b_size + index_in_palette_array * 4
        end = start + 4
        return start, end

    def get_pixel_indexes(self, index):
        pixel_size = math.ceil(self.parsed_image.bBitCount['value'] / 8)
        bf_off_bits = self.parsed_image.bfOffBits['value']
        start = bf_off_bits + (index - bf_off_bits) // pixel_size * pixel_size
        end = start + pixel_size
        return start, end

    def draw_palette_color(self, value):
        if value[3] == 0:
            value = (*value[:3], 255)
        color = QtGui.QColor(*value[:4])
        message = 'Palette color: rgba{}'.format(value)
        self.draw_color_rectangle(color, message)

    def draw_pixel(self, row, column):
        cell = self.hex_table.item(row, column)
        if not cell:
            return

        color = cell.background().color()
        tuple_color = (color.red(), color.green(),
                       color.blue(), color.alpha())
        self.draw_color_rectangle(
            color, 'Pixel color {}:'.format(tuple_color))

    def draw_color_rectangle(self, color, text):
        rect = QtGui.QPixmap(QtCore.QSize(40, 40))
        rect.fill(color)
        self.color_label.show()
        self.color_label.setPixmap(rect)
        self.cell_info_label.show()
        self.cell_info_label.setText(text)

    def parse_image(self):
        if not self.image_path:
            self.choose_file()
            self.image_path = os.path.abspath(self.image_path)

        if '.' not in os.path.split(self.image_path)[1]:
            show_warning_dialog('Not a file')
            self.image_path = None
            return

        self.parsed_image = bmp.get_image(self.image_path)
        image = self.parsed_image

        if isinstance(image, Exception) or isinstance(image, str):
            self.image_path = None
            show_warning_dialog(self.parsed_image)
            return

        self.image_path = None
        self.start()

    def start(self):
        self.parsed_image.get_format_report()
        self.prepare_table()
        self.prepare_image_area()
        self.start_thread()
        self.show_report()

    def prepare_image_area(self):
        self.image = QtGui.QImage(
            self.parsed_image.bWidth['value'],
            self.parsed_image.bHeight['value'], QtGui.QImage.Format_RGB32)
        self.image.fill(QtGui.QColor(255, 255, 255))
        self.image_pixmap = QtGui.QPixmap(self.image)
        self.image_label.setPixmap(self.image_pixmap)
        self.image_label.show()

    def prepare_table(self):
        self.clear_table()
        image_length = len(self.parsed_image.image_data)
        rows_count = image_length // self.hex_table_columns_count + 1
        self.hex_table.setRowCount(rows_count)
        self.hex_table.setColumnCount(self.hex_table_columns_count)

    def clear_table(self):
        self.hex_table.setRowCount(0)
        self.hex_table.setColumnCount(0)

    def show_report(self):
        messages = self.parsed_image.format_report.split('\n')
        self.report_messages_box.clear()
        for message in messages:
            self.report_messages_box.addItem(message)
        self.report_messages_box.show()

    def start_thread(self):
        if self.thread:
            self.thread.need_to_stop = True

        self.cell_info_label.clear()
        self.cell_info_label.hide()
        self.color_label.clear()
        self.color_label.hide()
        self.image_label.clear()
        self.progress_label.show()
        self.thread = Thread(self)
        self.thread.progress_bar_signal.connect(self.set_progress)
        self.thread.draw_pixels_line_signal.connect(self.draw_pixels_line)
        self.thread.finished.connect(self.thread_finished)
        self.thread.start()

    def set_progress(self, message):
        self.progress_label.setText(message)

    def draw_pixels_line(self):
        self.image_pixmap = QtGui.QPixmap(self.image)
        self.image_label.setPixmap(self.image_pixmap)

    def thread_finished(self):
        self.draw_pixels_line()
        self.hex_table.show()
        self.progress_label.hide()
        self.progress_label.clear()

    def choose_file(self):
        self.image_path = os.path.abspath(
            QtWidgets.QFileDialog.getOpenFileName()[0])


def set_color_to_label(label, color):
    rect = QtGui.QPixmap(QtCore.QSize(10, 10))
    rect.fill(color)
    label.setPixmap(rect)


def show_warning_dialog(error):
    message_text = str(error)
    message = QtWidgets.QMessageBox()
    message.setInformativeText(message_text)
    message.setIcon(QtWidgets.QMessageBox.Warning)
    message.setStandardButtons(QtWidgets.QMessageBox.Ok)
    message.show()
    message.exec_()


def parse_args():
    description = "Графическая версия программы для разбора формата BMP"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        'path', help='Path to BMP image', action='store', nargs='?')

    return parser.parse_args()


def main():
    args = parse_args()
    app = QtWidgets.QApplication(sys.argv)
    user_interface = MainWindow(args.path)
    user_interface.resize(300, 300)
    user_interface.show()
    user_interface.raise_()
    app.exec_()


if __name__ == '__main__':
    main()
